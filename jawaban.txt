No. 1
create database myshop;

No. 2
use myshop;
create table categories(
id int(8) auto_increment,
name varchar(255),
primary key(id)
);

create table items(
id int(8) auto_increment,
name varchar(255),
description varchar(255),
price int(10),
stock int(10),
category_id int(10),
primary key(id),
foreign key(category_id) references categories(id)
);

create table users(
id int(8) auto_increment,
name varchar(255),
email varchar(255),
password varchar(255),
primary key(id)
);

No. 3

insert into users values (1,"John Doe","john@doe.com","john123");
insert into users values (2,"Jane Doe","jane@doe.com","jenita123");

insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

insert into items(name, description, price, stock, category_id) values ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
insert into items(name, description, price, stock, category_id) values ("Uniklooh","baju keren dari brand ternama",500000,50,2);
insert into items(name, description, price, stock, category_id) values ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

No. 4

a.
select id, name, email from users;

b.
select * from items where price > 1000000;
select * from items where name like 'uniklo%';

c.
select items.id, items.name, items.description, items.category_id, categories.name from items
inner join categories on items.category_id = categories.id;

5.
update items set price = 2500000 where id = 1;